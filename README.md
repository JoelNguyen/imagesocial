# The Image Social
## Description
Image Social is a image-only social platform for people to upload
their image to discuss, create community.
## Core features
* Create discussion
* Upload post to response
* Infinity response to response system
* Social account
* and so much more
## Getting started
The project is divided into two separate applications, the back-end and front-end.
The back-end is the API service for front-end (React) to consume and use.
### Install Node
The project use the lastest Node.js version (node10),
[Click here](https://nodejs.org/en/) to download the latest node version
### Get API key
The back-end use three main technologies for post (image) management: 
- AWS S3 for storing the images 
- AWS Lambda and Serverless Framework: the backend divided into multiple functions (Lambda function).
In order to run the back-end locally, you will need [Serverless Framework](https://serverless.com/framework/docs/providers/aws/guide/quick-start/). 
You will also need AWS account to create [credentials](https://serverless.com/framework/docs/providers/aws/guide/credentials/). 
For assignment purpose, we will submit the API key on UTS online in the submission folder.
If you can't find it, please email [nguyenngocanh590@gmail.com](nguyenngocanh590@gmail.com) for all the API key.
- mLab: This is where we store our data.
- Put all your back-end or client API key in .env in the root folder of each application. For example backend/.env
### Install the library
- After install node, and serverless, you need to go to backend and client directory and run this command to install all the library

    ```npm install```


## The Back-end
### Run the back-end
- We use [serverless-offline](https://www.npmjs.com/package/serverless-offline) to run the functions on localhost with port 3000.
After install it, use this command to run the back-end

    ```sls offline```

    make sure that in the app.js file, exported handler function is
    ```javascript
  module.exports.handler = sls(app, {
            binary: ['image/png', 'image/jpeg', 'image/jpg', 'multipart/form-data', 'image/*']
       });
  ```
- Alternatively, you can use [nodemon](https://www.npmjs.com/package/nodemon) to run it on localhost:4000. Replace the exported handler function to
    ```javascript
    module.exports = app;
    ```
  and visit http://localhost:4000
  you can also change the post in the bin/www
  ```javascript
  var port = normalizePort(process.env.PORT || 'Your port');
  ```

### Structure
- app.js: Main file to run the application. 
- routes: Handle the routing.
- type: Hold the type of dependencies
- repository: The layers for the  database operations, business logic for pose and emoji.
- provider: The service provider for dependency injection
- middle: Hold the middle of our application
- helpers: Hold Helpers functions for validation
- controller: Our main application logic.

## The Front-end
The primary technologies used in the front-end are: Redux and React. We use
react-router-dom for routing in the client

### Run the client
- Use this command to run the client on localhost

```node
    npm start
```
## License
All copyright go to:
Ngoc Anh Nguyen - 12761767
Manh Cuong Tran - 12656132
Quoc Huy Nguyen - 12606923
Duc Hieu Pham - 12441376