export const SET_AUTHENTICATED = "SET_AUTHENTICATED";
export const SET_UNAUTHENTICATED = "SET_UNAUTHENTICATED";
export const LOGOUT = "SET_LOGOUT"
export const SIGNUP_ERROR = "SIGNUP_ERR"