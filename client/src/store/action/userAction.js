import { SET_AUTHENTICATED, SET_UNAUTHENTICATED, SIGNUP_ERROR } from "../type/userType";
import axios from "axios";
import history from '../../history'

//sent a login request to the server, require the user data to be passed in as parameter
//this function will send the request to the server under /api/auth/login with the user data as body.
//when the response come back, use the token in the response to set the axios header for further use.
//after that, redirect the user to homepage
//if there are any error (password/email is incorrect, etc), set the error inside the reducers
export const loginUser = (userData) => dispatch => {
  axios
    .post(process.env.REACT_APP_DEV_API + "/api/auth/login", { ...userData })
    .then(res => {
      setAuthourizationHeader(res.data.data.token);

      dispatch({ type: SET_AUTHENTICATED, payload: { ...res.data.data, error: '' } });
      history.push('/')
        })
    .catch(err => {
      if (err.response.data.error) {
        dispatch({
          type: SET_UNAUTHENTICATED,
          payload: { error: err.response.data.error }
        });
      }
    });
};

//sent a signup request to the server, require the user data to be passed in as parameter
//this function will send the request to the server under /api/auth/signup with the user data as body.
//when the response come back, use the token in the response to set the axios header for further use.
//after that, redirect the user to homepage
//if there are any error (password/email is incorrect, etc), set the error inside the reducers
export const signUpUser = (userData) => dispatch => {
  axios
    .post(process.env.REACT_APP_DEV_API + "/api/auth/register", { ...userData })
    .then(res => {
 
      
      setAuthourizationHeader(res.data.token);
      dispatch({ type: SET_AUTHENTICATED, payload: { ...res.data.token, signUpError: '', error: '' } });
      history.push('/')
    })
    .catch(err => {
      if (err.response.data.error) {
   
        dispatch({
          type: SIGNUP_ERROR,
          payload: { signUpError: err.response.data.error }
        });
      }
    });
};

export const error = message => dispatch => {
  dispatch({});
};

//this funcition will clear the the save token and remove the authorization header from axios
export const logoutUser = () => dispatch => {
  localStorage.removeItem("ImageSocialToken");
  delete axios.defaults.headers.common["Authorization"];
  dispatch({ type: SET_UNAUTHENTICATED });
};

//this function will set the authorization header for the axios for authentication reason.
//Require a token to be passed in
const setAuthourizationHeader = token => {
  console.log('set tolken', token)
  const JWTtoken = `JWT ${token}`;
  axios.defaults.headers.common["Authorization"] = JWTtoken;
};
