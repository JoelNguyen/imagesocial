import { FETCH_POSTS, ERROR_POST, ADD_PHOTO } from "../type/postType";
import axios from "axios";


// fetch all the posts
export const fetch_post_api = url => {
  return (dispatch, getState) => {
    axios
      .get(url)
      .then(res => {
        if (res.data) {
          dispatch({
            type: FETCH_POSTS,
            payload: {
              data: res.data
            }
          });
        }
      })
      .catch(err => console.log(err));
  };
};


// send the image
export const send_post_image = (url, data, callback) => {
  return (dispatch, getState) => {
    console.log(getState().userReducer.token)
    // const token = getState().useReducer.token
    const headers = {
      "Authorization": `JWT ${getState().userReducer.token}` 
    }

    axios.defaults.headers.post['Authorization'] = `JWT ${getState().userReducer.token}`  // for POST requests

    axios
      .post(url, data, headers)
      .then(res => {
        if (res) {
            callback()
        }
      })
      .catch(err => console.log(err.message));
  };
};


export const set_photo = (photo) => {
    return (dispatch, getState) => {
        dispatch({
            type: ADD_PHOTO,
            payload: {
                photo
            }
        })
      };
}

export const error = () => {
  return (dispatch, getState) => {
    dispatch({
      type: ERROR_POST,
      payload: {
        message: "Erorr"
      }
    });
  };
};

// delete
export const delete_post = id => {
  return (dispatch, getState) => {
    axios.delete("url").then(res => dispatch);
  };
};
