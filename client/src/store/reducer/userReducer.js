import { SET_AUTHENTICATED, SET_UNAUTHENTICATED, LOGOUT, SIGNUP_ERROR } from "../type/userType";
import User from "../../storage/User";
const initialState = {
  token: "",
  username: "",
  exp: "",
  _id: '',
  error: "",
  signUpError: "",
};

//when the this function is called (through dispatch) it will change the the initial state according to the action type
//this function require an action type to be passed in (through dispatch)
export default function(state = initialState, action) {
  switch (action.type) {
    case SET_AUTHENTICATED:
      User.save(action.payload);
      return {
        ...state,
        ...action.payload
      };
    case SET_UNAUTHENTICATED:
      return {
        ...state,
        error: action.payload.error
      };
    case SIGNUP_ERROR:
      return {
        ...state,
        signUpError: action.payload.signUpError
      }
    case LOGOUT:
      User.save(initialState);
      return {
        ...state,
        ...initialState
      };
    default:
      return state;
  }
}
