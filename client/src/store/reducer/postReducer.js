
import {FETCH_POSTS, ERROR_POST, ADD_PHOTO} from '../type/postType'
const initialState = {
    data: {
        docs: []
    },
    error: '',
    photo: null
}


const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_POSTS:
            return {
                ...state,
                data: action.payload.data
            }
        case ERROR_POST:
            return {
                ...state,
                error: action.payload.message
            }
        case ADD_PHOTO:
            return {
                ...state,
                photo: action.payload.photo
            }
        default:
            return state
    }
}

export default reducer