import postReducer from './postReducer'
import userReducer from './userReducer'
import { combineReducers } from 'redux'


const indexRoot = combineReducers({
    postReducer, userReducer
})

export default indexRoot