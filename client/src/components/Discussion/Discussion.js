import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import history from '../../history'
const Discussion = props => {
    return (
        <Paper className={props.classes.paper}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid className={props.classes.display} item xs>
                <Typography  variant="subtitle1">
                  {props.discussion.title}
                </Typography>
                <Typography variant="body2" >
                  {props.discussion.description}
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  Posted by user {props.discussion.user}
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  Create at {props.discussion.createdAt}
                </Typography>
              </Grid>
              <Grid className={props.classes.display} item>
                <Button variant="contained" 
                onClick={() => history.push(`/discussion/${props.discussion._id}`)}
                color="primary" className={props.classes.button}>
                Join
                {/* <Link type="button" style={{ color: 'white'}} to={`/discussion/${props.discussion._id}`}>Join</Link> */}
                </Button>
              </Grid>
            </Grid>
            
          </Grid>
        </Grid>
      </Paper>
    )
}

export default Discussion
