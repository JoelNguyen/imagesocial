import React, {useState, useEffect} from "react";
import {makeStyles} from "@material-ui/core/styles";

import axios from "axios";
import Discussion from "./Discussion";
import Pagination from "material-ui-flat-pagination";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        marginTop: 5
    },
    paper: {
        padding: theme.spacing(2),
        margin: "auto",
        marginTop: 5,
        maxWidth: 700
    },
    image: {
        width: 128,
        height: 128
    },
    img: {
        margin: "auto",
        display: "block",
        maxWidth: "100%",
        maxHeight: "100%"
    },
    button: {
        margin: theme.spacing(1)
    },
    fab: {
        position: "absolute",
        right: theme.spacing(3)
    },
    display: {
        textAlign: "center"
    },

    pagination: {
        textAlign: "center"
    }
}));

const DiscussionList = props => {
    const classes = useStyles();

    const [state, setState] = useState({
        discussions: []
    });

    const [offset, setOffSet] = useState(0);
    const [totalDocs, setTotalDocs] = useState(0);
    const [limit, setLimit] = useState(0);

    //comapre the two element in the array, if 1st element > 2nd element return -1, if 2nd element > 1st element return 1
    //else return 0
    //this function will check how many posts does each disucssion object has (posts[]) and return value accordingly based on the rule above
    //use this in the sort function .sort() to sort the dissucssion list (array of objects) by descending order based on number of posts
    function compare(a, b) {
        const postsA = a.posts.length;
        const postsB = b.posts.length;

        let comparision = 0;
        if (postsA > postsB) comparision = -1;
        else if (postsA < postsB) comparision = 1;

        return comparision;
    }

    useEffect(() => {
        fetch_api_discussion(1)
    }, []);

    const fetch_api_discussion = (page) => {
        axios
            .get(process.env.REACT_APP_DEV_API + "/api/discussion?page=" + page)
            .then(response => {
                console.log(response.data)
                const discussions = response.data.docs;
                setTotalDocs(response.data.totalDocs);
                setLimit(response.data.limit)
                discussions.sort(compare);
                setState({
                    ...state,
                    discussions
                });
            })
            .catch(error => {
                console.log(error);
            });
    }

    const handleClick = (e, offset, page) => {
        setOffSet(offset);
        fetch_api_discussion(page)

    }

    return (
        <div className={classes.root}>
            <h1 className={classes.display}>Discussion</h1>

            {state.discussions.map(discussion => (
                <Discussion
                    key={discussion._id}
                    discussion={discussion}
                    classes={classes}
                />
            ))}
            <div className={classes.pagination}>
                <Pagination
                    //requirement is limit only 10 items at a time
                    limit={limit}
                    offset={offset}
                    total={totalDocs}
                    onClick={handleClick}
                />
            </div>
        </div>
    );
};

export default DiscussionList;
