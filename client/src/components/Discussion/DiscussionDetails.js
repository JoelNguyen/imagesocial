import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Axios from "axios";

const styles = {
  paper: {
    padding: 20,
    marginTop: 20,
    marginLeft: 30,
    marginRight: 30
  },
  button: {
    marginTop: 20
  }
};

class DiscussionDetails extends Component {


  state = {
    createdAt: "",
    description: "",
    title: ""
  }

  componentDidMount() {
    Axios.get(
      process.env.REACT_APP_DEV_API +
        "/api/discussion/" +
        this.props.discussion_id
    )
      .then(res => {
        let date = this.getFullDate(res.data.createdAt)
        this.setState({
          createdAt: date,
          description: res.data.description,
          title: res.data.title
        })
      })
      .catch(err => console.log(err));
  }

   getFullDate = newDate => {
    var date = new Date(newDate);
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var dt = date.getDate();

    if (dt < 10) {
      dt = "0" + dt;
    }
    if (month < 10) {
      month = "0" + month;
    }

    let returnDate = year+'-' + month + '-'+dt
    return returnDate
  };

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.paper}>
        <Typography variant="h5" align="center">
          {this.state.title}
        </Typography>
        <br />
        <Typography variant="body2" align="left">
          {this.state.description}
        </Typography>
        <br />
        <Typography variant="body2" align="center">
          {this.state.createdAt}
        </Typography>
      </Paper>
    );
  }
}

export default withStyles(styles)(DiscussionDetails);
