import React from "react";
import {fade, makeStyles} from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import MoreIcon from "@material-ui/icons/MoreVert";
import {Link} from "react-router-dom";
import history from "../history";
import {useSelector, useDispatch} from "react-redux";
import {LOGOUT} from "../store/type/userType";

const useStyles = makeStyles(theme => ({
    grow: {
        flexGrow: 1
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        display: "none",
        [theme.breakpoints.up("sm")]: {
            display: "block"
        }
    },
    search: {
        position: "relative",
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        "&:hover": {
            backgroundColor: fade(theme.palette.common.white, 0.25)
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: "100%",
        [theme.breakpoints.up("sm")]: {
            marginLeft: theme.spacing(3),
            width: "auto"
        }
    },
    searchIcon: {
        width: theme.spacing(7),
        height: "100%",
        position: "absolute",
        pointerEvents: "none",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    inputRoot: {
        color: "inherit"
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create("width"),
        width: "100%",
        [theme.breakpoints.up("md")]: {
            width: 200
        }
    },
    sectionDesktop: {
        display: "none",
        [theme.breakpoints.up("md")]: {
            display: "flex"
        }
    },
    sectionMobile: {
        display: "flex",
        [theme.breakpoints.up("md")]: {
            display: "none"
        }
    }
}));

export default function PrimarySearchAppBar() {
    const token = useSelector(state => state.userReducer.token || "");
    const username = useSelector(state => state.userReducer.username || "")

    const dispatch = useDispatch()

    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

    const isMenuOpen = Boolean(anchorEl);
    Boolean(mobileMoreAnchorEl);

    function handleProfileMenuOpen(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleMobileMenuClose() {
        setMobileMoreAnchorEl(null);
    }

    function handleMenuClose() {
        setAnchorEl(null);
        handleMobileMenuClose();
    }

    const menuId = "primary-search-account-menu";
    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{vertical: "top", horizontal: "right"}}
            id={menuId}
            keepMounted
            transformOrigin={{vertical: "top", horizontal: "right"}}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >

            {token.length > 0 ? (
                <React.Fragment>
                    <MenuItem onClick={() => {
                        history.push('/posted')
                        handleMenuClose()
                    }}>My Posts</MenuItem>

                    <MenuItem onClick={() => {
                        history.push('/add')
                        handleMenuClose()
                    }}>Create Discussion</MenuItem>


                    <MenuItem onClick={() => {
                        dispatch({type: LOGOUT})
                        handleMenuClose()
                    }}>Logout</MenuItem>
                </React.Fragment>


            ) : (
                <React.Fragment>
                    <MenuItem
                        onClick={() => {
                            history.push("/login");
                            handleMenuClose();
                        }}
                    >
                        Login
                    </MenuItem>
                    <MenuItem
                        onClick={() => {
                            history.push("/signup");
                            handleMenuClose();
                        }}
                    >
                        Sign Up
                    </MenuItem>

                </React.Fragment>
            )}
        </Menu>
    );

    const mobileMenuId = "primary-search-account-menu-mobile";


    return (
        <div className={classes.grow}>
            <AppBar position="static" xs={12} sm={12}>
                <Toolbar>
                    <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="open drawer"
                    >

                    </IconButton>
                    <Typography variant="h6" noWrap>
                        <Link style={{color: "white"}} to={`/`}>
                            Image Social
                        </Link>
                    </Typography>

                    <div className={classes.grow}/>

                    <div>
                        <IconButton
                            aria-label="show more"
                            aria-controls={mobileMenuId}
                            aria-haspopup="true"
                            onClick={handleProfileMenuOpen}
                            color="inherit"
                        >
                            <MoreIcon/>
                        </IconButton>
                    </div>
                    <a style={{color: "white"}}>
                        {username}
                    </a>
                </Toolbar>

            </AppBar>

            {/* {renderMobileMenu} */}
            {renderMenu}
        </div>
    );
}
