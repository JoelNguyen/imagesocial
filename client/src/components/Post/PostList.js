import React from 'react'
import {useSelector} from 'react-redux'
import Post from './Post';
const PostList = () => {


    const posts = useSelector(state => state.postReducer.data.docs)
    
    return posts.map((post) => <Post key={post._id} post={post}/>)
}

export default PostList
