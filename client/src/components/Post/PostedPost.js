import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import history from "../../history";
import axios from "axios";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
import FileUpload from "../FileUpload";

const styles = {
  card: {
    marginTop: 20,
    marginLeft: 30,
    marginRight: 30
  }
};

const PostedPost = props => {
  const [show, setShow] = React.useState(false);

  const [photo, setPhoto] = React.useState(null);

  const upload = e => {
    e.preventDefault();

    const data = new FormData(e.target);

    const post_id = props.id;


    data.append("photo", photo);

    data.append("post_id", post_id);

    axios.defaults.headers.put["Authorization"] = `JWT ${props.token}`; // for POST requests

    axios.put(`${process.env.REACT_APP_DEV_API}/api/post`, data)
    .then(res => {
        setShow(false)
        setPhoto(null)
        props.fetch_api_post();

    })
    .catch(err =>  NotificationManager.error("Can't change post, please try again", "Error")) 

    

    // const post_id = req.body.post_id;
    // const user_id = req.user._id;
    // const old_image_key = req.body.image_key;

    
  };

  const { classes } = props;

  const handleDelete = e => {
    axios.defaults.headers.delete["Authorization"] = `JWT ${props.token}`; // for POST requests

    axios
      .delete(`${process.env.REACT_APP_DEV_API}/api/post`, {
        data: {
          post_id: props.id,
          discussion_id: props.discussion_id,
          image_key: props.image_key
        }
      })
      .then(res => {
        //refetch the post
        props.fetch_api_post();
      })
      .catch(err => {
        console.log(err.response);
        if (err.response) {
          NotificationManager.error(err.response.data, "Error");
        }
      });
  };

  return (
    <Grid container>
      <Grid item xs={12}>
        <Card className={classes.card}>
          <CardMedia
            onClick={() => {
              history.push(
                `/discussion/${props.discussion_id}/post/response/${props.id}`
              );
            }}
            style={{ cursor: "pointer" }}
            component="img"
            image={props.image_url}
            maxheight="500"
          />
          <CardActions>
            <Button
              onClick={handleDelete}
              variant="contained"
              color="secondary"
            >
              Delete
            </Button>

            <Button 
            onClick={() => setShow(true)}
            variant="contained" className={classes.button}>
              Change
            </Button>

            <span style={{ color: "grey", fontSize: "0.8rem" }}>
              {props.date}
            </span>

            <FileUpload
                changephoto={true}
              setPhoto={setPhoto}
              upload={upload}
              setShow={setShow}
              show={show}
            />

          </CardActions>
        </Card>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(PostedPost);
