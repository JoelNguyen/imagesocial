import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center"
  },
  section: {
    height: 500,
    paddingTop: 5
  },
  button: {
    margin: theme.spacing(1),
    marginTop: 20,
    marginLeft: 30,
    marginRight: 30
  },
  input: {
    display: "none"
  },
}));

const PostsBox = props => {
  const classes = useStyles();

  // const [show, setShow] = useState(false);


  return (
    <div className={classes.root}>
      {props.children}
    </div>
  );
};

export default PostsBox;
