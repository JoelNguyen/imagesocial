import React, { useState } from "react";
import { Grid, Paper } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import PostsBox from "./PostsBox";
import { useDispatch } from "react-redux";
import PostList from "./PostList";
import { FETCH_POSTS } from "../../store/type/postType";
import axios from "axios";
import {
  fetch_post_api,
  send_post_image
} from "../../store/action/postActions";
import FileUpload from "../FileUpload";
import Post from "./Post";

const styles = theme => ({
  paper: {
    padding: theme.spacing(3),
    overflowY: "auto",
    [theme.breakpoints.up("sm")]: {
      marginTop: 5,
      height: "calc(100% - 10px)"
    },
    [theme.breakpoints.down("xs")]: {
      height: "100%"
    }
  },
  "@global": {
    "html, body, #root": {
      height: "100%"
    }
  },
  container: {
    [theme.breakpoints.up("sm")]: {
      height: "calc(100% - 64px - 48px)"
    },
    [theme.breakpoints.down("xs")]: {
      height: "calc(100% - 56px - 48px)"
    }
  },
  parentContainer: {
    marginBottom: '2rem'
  },
  item: {
    [theme.breakpoints.down("xs")]: {
      height: "50%"
    }
  }
});

const PostResponsePage = props => {
  const { classes } = props;

  const [show, setShow] = useState(false);

  const dispatch = useDispatch();

  const [parent_post, set_parent_post] = React.useState({});

  const [photo, setPhoto] = React.useState(null);

  const upload = e => {
    e.preventDefault();

    const data = new FormData(e.target);

    const parent_id = props.match.params.post_id;

    const discussion_id = props.match.params.id;

    data.append("photo", photo);

    data.append("discussion_id", discussion_id);

    data.append("parent_post", parent_id);

    dispatch(
      send_post_image(
        process.env.REACT_APP_DEV_API + "/api/post/response",
        data,
        () => {
          // set null to clean the form upload photo
          setPhoto(null);
          // set false to closes the modal
          setShow(false);
          // fetch the api again in the future, don't do this

          const parent_id = props.match.params.post_id;
          const discussion_id = props.match.params.id;
          const url =
            process.env.REACT_APP_DEV_API +
            "/api/post/response/" +
            discussion_id +
            "/" +
            parent_id;
          dispatch(fetch_post_api(url));
        }
      )
    );
  };

  // fetch api
  React.useEffect(() => {
    const parent_id = props.match.params.post_id;
    const discussion_id = props.match.params.id;
    const url =
      process.env.REACT_APP_DEV_API +
      "/api/post/response/" +
      discussion_id +
      "/" +
      parent_id;
    dispatch(fetch_post_api(url));

    return function cleanup() {
      const data = {
        docs: []
      };
      dispatch({
        type: FETCH_POSTS,
        payload: {
          data: data
        }
      });
    };
  }, [props.match.params.post_id, props.match.params.id]);

  // fetch the parent detail
  React.useEffect(() => {
    const parent_id = props.match.params.post_id;
    const discussion_id = props.match.params.id;

    axios
      .get(
        process.env.REACT_APP_DEV_API +
          `/api/post/detail/parent/response/${parent_id}`
      )
      .then(res => {
        set_parent_post(res.data);
      })
      .catch(err => console.log(err));

    return function cleanup() {
      set_parent_post({});
    };
  }, [props.match.params.post_id, props.match.params.id]);

  return (
    <div>
      <Grid container className={classes.container}>
        <Grid
          // onClick={() => {
          //   history.goBack()
          // }}
          className={classes.parentContainer}
          item
          className={classes.item}
          xs={12}
          sm={3}
        >
          <Post post={parent_post} />
          
        </Grid>
        <Grid item xs={12} sm={2} />

        <Grid item className={classes.item} xs={12} sm={5}>
          <Paper className={classes.paper}>
            <PostsBox
              discussion_id={props.match.params.id}
              parent_post={props.match.params.post_id}
            >
              <PostList />
            </PostsBox>
            <FileUpload
              setPhoto={setPhoto}
              upload={upload}
              setShow={setShow}
              show={show}
            />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles)(PostResponsePage);
