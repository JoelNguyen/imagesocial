import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { useSelector } from "react-redux";

import axios from "axios";
const PostEmoji = props => {
  const [emojis, setEmojis] = React.useState([]);

  const token = useSelector(state => state.userReducer.token || "");

  const [btnStatus, setBtnStatus] = React.useState("");
  React.useEffect(() => {
    axios
      .get(process.env.REACT_APP_DEV_API + "/api/post/reaction/emoji")
      .then(res => {
        setEmojis(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  const [state, setState] = React.useState({
    anchorEl: null,
    open: false
  });

  const handleClick = event => {
    setState({ open: true, anchorEl: event.currentTarget });
  };
  const handleClose = () => {
    setState({ open: false });
  };

  const send_reaction = (id, name) => {
    // router.post('/reaction/:post_id/:discussion_id/:emoji_id', passport.authenticate("jwt", { session: false }),PostController.reaction)

    const post_id = props.post._id;
    const discussion = props.post.discussion;

    const url = `${process.env.REACT_APP_DEV_API}/api/post/reaction/${post_id}/${discussion}/${id}`;

    axios.defaults.headers.post["Authorization"] = `JWT ${token}`; // for POST requests

    axios
      .post(url, { name: name })
      .then(res => {
        console.log(res);
      })
      .catch(err => console.log(err));

 
      handleClose();

      if(name === btnStatus) {
        setBtnStatus('React')
            return 
      }
        setBtnStatus(name);

  };

  React.useEffect(() => {
    axios.defaults.headers.get["Authorization"] = `JWT ${token}`; // for POST requests

    const post_id = props.post._id;

    const url = `${process.env.REACT_APP_DEV_API}/api/post/reaction/find/${post_id}`;

    axios
      .get(url)
      .then(res => {
        if (res.data.name) {
          setBtnStatus(res.data.name);
        } else {
          setBtnStatus("React");
        }
      })
      .catch(err => console.log(err));
  }, []);

  return emojis.length > 0 ? (
    <React.Fragment>
      <Button size="small" color="primary" onClick={handleClick}>
        {btnStatus}
      </Button>

      <Menu
        id="simple-menu"
        anchorEl={state.anchorEl}
        open={state.open}
        onClose={handleClose}
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        transformOrigin={{ vertical: "top", horizontal: "center" }}
      >
        {emojis.map(emoji => {
          return (
            <MenuItem
              key={emoji._id}
              onClick={() => send_reaction(emoji._id, emoji.name)}
            >
              {emoji.name}
            </MenuItem>
          );
        })}
      </Menu>
    </React.Fragment>
  ) : (
    <React.Fragment></React.Fragment>
  );
};

export default PostEmoji;
