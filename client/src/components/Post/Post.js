import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import history from "../../history";
import {connect} from "react-redux";
import PostEmoji from "./PostEmoji";

const styles = {
    card: {
        marginTop: 20,
        marginLeft: 30,
        marginRight: 30
    }
};

class Post extends Component {
    state = {
        anchorEl: null,
        open: false
    };

    handleClick = event => {
        this.setState({open: true, anchorEl: event.currentTarget});
    };
    handleClose = () => {
        this.setState({open: false});
    };

    getFullDate = newDate => {
        var date = new Date(newDate);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var dt = date.getDate();

        if (dt < 10) {
            dt = "0" + dt;
        }
        if (month < 10) {
            month = "0" + month;
        }

        let returnDate = year + "-" + month + "-" + dt;
        return returnDate;
    };

    render() {
        const {
            classes,
            post: {createdAt, image_url, discussion, _id}
        } = this.props;

        const date = this.getFullDate(createdAt);

        return (
            <Grid container>
                <Grid item xs={12}>
                    <Card className={classes.card}>
                        <CardMedia
                            style={{cursor: "pointer"}}
                            onClick={() =>
                                history.push(`/discussion/${discussion}/post/response/${_id}`)
                            }
                            component="img"
                            image={image_url}
                            maxheight="500"
                            alt="Response Post"

                        />
                        <CardActions>


                            {/* REACTION */}
                            {this.props.token.length > 0 && <PostEmoji post={this.props.post}/>}

                            <span style={{color: "grey", fontSize: "0.8rem"}}>{date}</span>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => ({
    token: state.userReducer.token
});

const mapActionsToProps = {};

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles)(Post));
