import React from "react";
import { DropzoneArea } from "material-ui-dropzone";
import { makeStyles } from "@material-ui/core/styles";
import { Modal, Button, Grid } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { set_photo } from "../store/action/postActions";
const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  margin: {
    margin: theme.spacing(1)
  },
  withoutLabel: {
    marginTop: theme.spacing(3)
  },
  textField: {
    flexBasis: 200
  },
  textDropzone: {
    padding: "1rem",
    color: "grey"
  }
}));

const FileUpload = props => {
  const classes = useStyles();

  const dispatch = useDispatch();

  console.log(props.changephoto)
  return (
    <Grid container style={{ marginTop: "2rem", marginLeft: "1.8rem" }}>
     {props.changephoto ? <React.Fragment></React.Fragment> : <Grid item xs={6}>
        <Button
          variant="contained"
          size="large"
          color="primary"
          className={classes.button}
          onClick={() => props.setShow(true)}
        >
          Upload!
        </Button>
      </Grid> }

      <Modal
        open={props.show}
        onClose={() => {
          props.setShow(false);
        }}
        className={classes.margin}
      >
        <Grid
          direction="column"
          style={{ outline: "none" }}
          container
          justify="center"
          alignItems="center"
        >
          <Grid item xs={6} style={{ backgroundColor: "white" }}>
            <form onSubmit={props.upload}>
              <DropzoneArea
                showPreviews={true}
                showPreviewsInDropzone={false}
                dropzoneParagraphClass={classes.textDropzone}
                filesLimit={1}
                acceptedFiles={["image/jpeg", "image/png", "image/bmp"]}
                onChange={e => {
                  props.setPhoto(e[0]);
                  dispatch(set_photo(e[0]));
                }}
              />

              <Button
                type="submit"
                style={{ marginTop: "1rem", width: "100%" }}
                variant="outlined"
                color="primary"
              >
                Submit
              </Button>
            </form>
          </Grid>
        </Grid>
      </Modal>
    </Grid>
  );
};

export default FileUpload;
