import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { Grid } from "@material-ui/core";
import axios from "axios";
const useStyles = makeStyles(theme => ({
  root: {
    marginTop: 5,
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  title: {
    textAlign: "center"
  }
}));

export default function LeaderBoard() {
  const classes = useStyles();

  const [topUser, setTopUser] = React.useState([]);

  React.useEffect(() => {
    axios.get(`${process.env.REACT_APP_DEV_API}/api/post/user/leaderboard`).then(res => {
        let setUser = []
      if (res.data) {
        res.data.forEach((user) => {
            if(user.posted) {
                setUser.push(user)
            }
        })
        setTopUser(setUser)
        
      }
    });
  }, []);

  return (
    <Grid>
      <Grid className={classes.title}>
        <h1>Leader Broad</h1>
      </Grid>
      <Grid container direction="row" justify="center" alignItems="center">
        <List
          component="nav"
          className={classes.root}
          aria-label="mailbox folders"
        >
          {topUser.length > 0 &&
            topUser.map(user => {
              return (
                <ListItem key={user._id} button>
                  <ListItemText primary={`${user.name}: ${user.posted}`} />
                </ListItem>
              );
            })}
          {/* <ListItem button>
                    <ListItemText primary="Inbox" />
                </ListItem>
                <Divider />
                <ListItem button divider>
                    <ListItemText primary="Drafts" />
                </ListItem>
                <ListItem button>
                    <ListItemText primary="Trash" />
                </ListItem>
                <Divider light />
                <ListItem button>
                    <ListItemText primary="Spam" />
                </ListItem>
                <ListItem button>
                    <ListItemText primary="Talk" />
                </ListItem> */}
        </List>
      </Grid>
    </Grid>
  );
}
