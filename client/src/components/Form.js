import React, {Component} from "react";
import Joi from "joi-browser";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

class Form extends Component {
    state = {
        data: {},
        errors: {}
    }

    validate = () => {
        const {error} = Joi.validate(this.state.data, this.schema, {abortEarly: false});
        // console.log({error})

        if (!error) return null;


        const errors = {};

        //convert array data to object
        for (let item of error.details) {
            errors[item.path[0]] = item.message;
        }

        return errors;

    }

    //check validate in a property
    validateProperty = ({name, value}) => {
        const obj = {[name]: value};
        const schema = {[name]: this.schema[name]};
        const {error} = Joi.validate(obj, schema);
        return error ? error.details[0].message : null
    }

    //submit form
    handleSubmit = e => {
        e.preventDefault();
        const errors = this.validate();

        this.setState({errors: errors || {}});
        if (errors) return;
        this.doSubmit();
    }


    //handle input form
    handleChange = ({currentTarget: input}) => {

        //handle validate in a property
        const errors = {...this.state.errors};
        const errorMessage = this.validateProperty(input);
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];

        //handle data when click on submit button
        const data = {...this.state.data};
        data[input.name] = input.value;


        this.setState({data, errors});
    }

    renderButton(label) {
        const {classes} = this.props;
        return (
            <Button
                disabled={this.validate()}
                type="submit"
                variant="contained"
                color="primary"
                className={classes.button}
            >
                {label}
            </Button>
        )
    }

    renderInput(name, label, type) {
        const {classes} = this.props;
        const {data, errors} = this.state;
        return (
            <TextField
                value={data[name]}
                onChange={this.handleChange}
                error={errors[name]}
                id={name}
                name={name}
                type={type}
                label={label}
                className={classes.textField}
                fullWidth
            />
        )
    }

    renderError(name) {
        const {errors} = this.state;
        return (
            <div>
                {errors[name] && <div>{errors[name]}</div>}
            </div>

        )
    }
}

export default Form;