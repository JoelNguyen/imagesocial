import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";

import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {signUpUser} from "../store/action/userAction";
import PropTypes from "prop-types";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Form from "../components/Form";
import Joi from "joi-browser";
import history from '../history'

const styles = theme => ({
    ...theme.spreadThis
});

class signup extends Form {
    state = {
        data: {name: "", email: "", password: ""},
        errors: {}
    };

    schema = {
        name: Joi.string()
            .label("Name")
            .required()
            .min(3)
            .max(20),

        email: Joi.string().email().required()
            .label("Email"),

        password: Joi.string()
            .required()
            .label("Password")
            .regex(/^[a-zA-Z0-9]{5,15}$/)
    };

    // submit the registration
    doSubmit() {
        //get data from the state and pass it into the signup function
        const userData = {
            name: this.state.data.name,
            email: this.state.data.email,
            password: this.state.data.password
        };

        try {
            this.props.signUpUser(userData, this.props.history);

        } catch (e) {
            // console.log("EMAIWEFALWEFL")
        }
    }

    //redirect the user if the user already logged in
    componentWillMount() {
        if (this.props.token) {
            if (this.props.token.length > 0) {
                history.push('/')
            }
        }
        // if (this.props.token.length > 0) return <Redirect to="/"/>
    }

    render() {
        const {classes} = this.props;
        return (
            <Grid container className={classes.form}>
                <Grid item xs/>
                <Grid item xs>
                    <Typography variant="h3" className={classes.pageTitle}>
                        Sign Up
                    </Typography>

                    <form onSubmit={this.handleSubmit}>
                        {this.renderInput("name", "Name", "name")}
                        {this.renderError("name")}
                        {this.renderInput("email", "Email", "email")}
                        {this.renderError("email")}

                        {this.renderInput("password", "Password", "password")}
                        {this.renderError("password")}

                        {this.props.signUpError && <div style={{color: 'red'}}>{this.props.signUpError}</div>}
                        {this.renderButton("Signup")}
                        <br/>
                        <small>
                            Already have an account? Login <Link to="/login">here</Link>
                        </small>
                    </form>
                </Grid>
                <Grid item xs/>
            </Grid>
        );
    }
}

//get the signup function from redux actions as props for further use
signup.propTypes = {
    signUpUser: PropTypes.func.isRequired
};

//get the user redux state as props
const mapStateToProps = state => ({
    signUpError: state.userReducer.signUpError,
    token: state.userReducer.token
});

//get the signup function as props
const mapActionsToProps = {
    signUpUser
};

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles)(signup));
