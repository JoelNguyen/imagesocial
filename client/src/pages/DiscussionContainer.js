import React from 'react';

import DiscussionList from '../components/Discussion/DiscussionList';
import LeaderBroad from '../components/Leaderboard/LeaderBroad';

function DiscussionContainer() {
    return (
      <div className="Homepage">
          <LeaderBroad />
          <DiscussionList />
      </div>
    );
  }
  
  export default DiscussionContainer;