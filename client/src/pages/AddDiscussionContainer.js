import React from 'react';
import Grid from '@material-ui/core/Grid'
import {makeStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import axios from 'axios';
import {useSelector} from 'react-redux'
import joi from 'joi-browser'

const useStyles = makeStyles(theme => ({
    container: {
        display: 'block',
        textAlign: "center"
    },
    form: {
        textAlign: "center"
    },
    textField: {
        margin: "10px auto 10px auto",
        width: 500,
    },
    pageTitle: {
        margin: "10px auto 10px auto"
    },
    button: {
        margin: theme.spacing(1),
        width: 80
    },

}));


const AddDiscussionContainer = props => {
    const classes = useStyles();
    const token = useSelector(state => state.userReducer.token || "");

    const [values, setValues] = React.useState({
        postID: '',
        title: '',
        description: '',
        createdDate: '',
        errors: {}
    });


    const schema = {
        title: joi.string().min(15).max(50).required(),
        description: joi.string().max(200).min(5)
    }


    const validate = () => {

        const data = {
            title: values.title,
            description: values.description
        }

        const {error} = joi.validate(data, schema, {abortEarly: false});

        if (!error) return null;

        const errors = {};

        //convert array data to object
        for (let item of error.details) {
            errors[item.path[0]] = item.message;
        }
        return errors;

    }


    //  check validate in a property
    const validateProperty = ({name, value}) => {
        const obj = {[name]: value};
        const sche = {[name]: schema[name]};
        const {error} = joi.validate(obj, sche);
        return error ? error.details[0].message : null
    }


    const renderError = (name) => {
        const {errors} = values;
        return (
            <div>
                {errors[name] && <div>{errors[name]}</div>}
            </div>

        )
    }


    const handleSubmit = (e) => {


        e.preventDefault()
        const error = validate()

        console.log(error)

        const validateTitle = {
            name: 'title',
            value: values.title
        }


        const validateDescription = {
            name: 'description',
            value: values.description
        }
        const errorMessageTitle = validateProperty(validateTitle)
        const errorMessageDescription = validateProperty(validateDescription)


        setValues({
            ...values,
            errors: {
                'title': errorMessageTitle,
                'description': errorMessageDescription
            }
        })


        if (!error) {

            const discussion = {
                title: values.title,
                description: values.description,
            };

            axios.defaults.headers.post["Authorization"] = `JWT ${token}`;


            axios.post(`${process.env.REACT_APP_DEV_API}/api/discussion`, {discussion})
                .then(res => {
                    if (res) {
                        props.history.push('/')
                    }
                })
        }
    }


    return (
        <div>
            <Grid container justify="center">
                <Typography variant="h4" className={classes.pageTitle}>
                    Create a post
                </Typography>
            </Grid>
            <Grid
                container
                spacing={0}
                alignItems="center"
                direction="column"
                justify="center"
            >
                <form onSubmit={handleSubmit} className={classes.container} autoComplete="on">

                    <TextField
                        required
                        id="standard-required"
                        name="title"
                        label="Topic"
                        placeholder="Required"
                        className={classes.textField}
                        margin="normal"
                        onChange={(e) => setValues({
                            ...values,
                            title: e.target.value,

                        })}
                    />

                    {renderError('title')}

                    <br/>

                    <small>
                        You must have at least 15 charecters in your title
                    </small>
                    <br/>

                    <TextField
                        id="standard-multiline-static"
                        name="description"
                        label="Description"
                        multiline
                        placeholder="The content of this topic should talk about..."
                        className={classes.textField}
                        margin="normal"
                        onChange={(e) => setValues({
                            ...values,

                            description: e.target.value
                        })}
                    />

                    {renderError('description')}

                    <Grid container justify="center">
                        <Button variant="contained"

                                type="submit"
                            // onClick={() => handleSubmit()}
                                color="primary" className={classes.button}>
                            Create
                        </Button>
                    </Grid>
                </form>
            </Grid>

        </div>
    );

}

export default AddDiscussionContainer