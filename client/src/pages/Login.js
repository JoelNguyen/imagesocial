import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import {Link} from "react-router-dom";
import Joi from "joi-browser";
import PropTypes from "prop-types";
import {Redirect} from "react-router-dom";

import {connect} from "react-redux";
import {loginUser} from "../store/action/userAction";

//Mui
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Form from "../components/Form";

const styles = theme => ({
    ...theme.spreadThis
});

class login extends Form {
    state = {
        data: {email: "", password: ""},
        errors: {}
    };

    schema = {
        email: Joi.string()
            .required()
            .email()
            .label("Email"),
        password: Joi.string()
            .required()
            .label("Password")
            .regex(/^[a-zA-Z0-9]{5,15}$/)
    };

    doSubmit = () => {
        //call server
        //get data from the state and pass it into the login function
        const userData = {
            email: this.state.data.email,
            password: this.state.data.password
        };
        this.props.loginUser(userData, this.props.history);
    };

    render() {
        const {classes} = this.props;

        //redirect the user if the user already logged in
        if (this.props.token.length > 0) return <Redirect to="/"/>

        return (
            <Grid container className={classes.form}>
                <Grid item xs/>
                <Grid item xs>
                    <Typography variant="h3" className={classes.pageTitle}>
                        Login
                    </Typography>
                    <form onSubmit={this.handleSubmit}>
                        {this.renderInput("email", "Email", "email")}
                        {this.renderError("email")}
                        {this.renderInput("password", "Password", "password")}
                        {this.renderError("password")}

                        {this.props.error.length > 0 && (
                            <div style={{color: "red"}}>{this.props.error}</div>
                        )}

                        {this.renderButton("Login")}

                        <br/>
                        <small>
                            Don't have an date? Sign up <Link to="/signup">here</Link>
                        </small>
                    </form>
                </Grid>
                <Grid item xs/>
            </Grid>
        );
    }
}

//get the login function from redux actions as props for further use
login.propTypes = {
    loginUser: PropTypes.func.isRequired
};

//get the user redux state as props
const mapStateToProps = state => ({
    error: state.userReducer.error,
    token: state.userReducer.token
});

//get the login function as props
const mapActionsToProps = {
    loginUser
};


export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles)(login));
