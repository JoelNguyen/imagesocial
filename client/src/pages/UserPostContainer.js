import React from "react";
import {Grid, Paper} from "@material-ui/core";
import {withStyles} from "@material-ui/core/styles";
import PostsBox from "../components/Post/PostsBox";
import {useSelector} from "react-redux";
import axios from "axios";
import {NotificationManager} from 'react-notifications'
import PostedPost from "../components/Post/PostedPost";

const styles = theme => ({
    paper: {
        padding: theme.spacing(3),
        overflowY: "auto",
        [theme.breakpoints.up("sm")]: {
            marginTop: 5,
            height: "calc(100% - 10px)"
        },
        [theme.breakpoints.down("xs")]: {
            height: "100%"
        }
    },
    "@global": {
        "html, body, #root": {
            height: "100%"
        }
    },
    container: {
        [theme.breakpoints.up("sm")]: {
            height: "calc(100% - 64px - 48px)"
        },
        [theme.breakpoints.down("xs")]: {
            height: "calc(100% - 56px - 48px)"
        }
    },
    parentContainer: {
        marginBottom: "2rem"
    },
    item: {
        [theme.breakpoints.down("xs")]: {
            height: "50%"
        }
    }
});

const UserPostContainer = props => {
    const {classes} = props;

    const [postedposts, setpostedposts] = React.useState([]);

    const token = useSelector(state => state.userReducer.token || "");


    const fetch_api_post = () => {
        axios.defaults.headers.get["Authorization"] = `JWT ${token}`; // for POST requests

        axios
            .get(`${process.env.REACT_APP_DEV_API}/api/post/user/posted`)
            .then(res => {
                setpostedposts(res.data);
            })
            .catch(err => {
                NotificationManager.error("We can't get the your post, please try again", "Error")
            });
    }

    // fetch api
    React.useEffect(() => {


        // eslint-disable-next-line react-hooks/exhaustive-deps
        fetch_api_post();
    }, []);


    return (
        <div>
            <Grid container className={classes.container}>
                <Grid item xs={12} sm={2}/>

                <Grid item className={classes.item} xs={12} sm={5}>
                    <Paper className={classes.paper}>
                        <PostsBox
                            discussion_id={props.match.params.id}
                            parent_post={props.match.params.post_id}
                        >
                            {/* <PostList /> */}
                            {postedposts.length > 0 &&
                            postedposts.map(post => (
                                <PostedPost
                                    token={token}
                                    id={post._id}
                                    discussion_id={post.discussion}
                                    image_key={post.image_key}
                                    image_url={post.image_url}
                                    fetch_api_post={fetch_api_post}
                                />
                            ))}
                        </PostsBox>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
};

export default withStyles(styles)(UserPostContainer);
