import React, {useState} from "react";
import {Grid, Paper} from "@material-ui/core";
import {withStyles} from "@material-ui/core/styles";
import PostsBox from "../components/Post/PostsBox";
import DiscussionDetails from "../components/Discussion/DiscussionDetails";
import PostList from "../components/Post/PostList";
import {FETCH_POSTS} from "../store/type/postType";
import {useDispatch} from "react-redux";
import FileUpload from "../components/FileUpload";
import Pagination from "material-ui-flat-pagination";
import {
    fetch_post_api,
    send_post_image
} from "../store/action/postActions";
import {useSelector} from 'react-redux'

const styles = theme => ({
    paper: {
        padding: theme.spacing(3),
        overflowY: "auto",
        [theme.breakpoints.up("sm")]: {
            marginTop: 5,
            height: "calc(100% - 10px)"
        },
        [theme.breakpoints.down("xs")]: {
            height: "100%"
        }
    },
    "@global": {
        "html, body, #root": {
            height: "100%"
        }
    },
    container: {
        [theme.breakpoints.up("sm")]: {
            height: "calc(100% - 64px - 48px)"
        },
        [theme.breakpoints.down("xs")]: {
            height: "calc(100% - 56px - 48px)"
        }
    },
    item: {
        [theme.breakpoints.down("xs")]: {
            height: "50%"
        }
    }
});

const PostContainer = props => {

    const [show, setShow] = useState(false);

    const totalDocs = useSelector(state => state.postReducer.data.totalDocs)

    const limit = useSelector(state => state.postReducer.data.limit)

    const [offset, setOffset] = useState(0);

    const token = useSelector(state => state.userReducer.token || '')

    const {classes} = props;

    const dispatch = useDispatch();

    const [photo, setPhoto] = useState(null);

    const upload = e => {
        e.preventDefault();
        const data = new FormData(e.target);

        const discussion_id = props.match.params.id;

        data.append("photo", photo);

        data.append("discussion_id", discussion_id);

        dispatch(
            send_post_image(process.env.REACT_APP_DEV_API + "/api/post", data, () => {
                // set null to clean the form upload photo
                setPhoto(null);
                // set false to closes the modal
                setShow(false);
                // fetch the api again in the future, don't do this
                dispatch(
                    fetch_post_api(
                        process.env.REACT_APP_DEV_API + "/api/post/" + discussion_id
                    )
                );
            })
        );
    };

    React.useEffect(() => {
        if (props.match.params.id)
            dispatch(
                fetch_post_api(
                    process.env.REACT_APP_DEV_API + "/api/post/" + props.match.params.id
                )
            );

        return function cleanup() {
            const data = {
                docs: []
            };
            dispatch({
                type: FETCH_POSTS,
                payload: {
                    data: data
                }
            });
        };
    }, []);

    const handleClick = (e, offset, page) => {

        console.log()
        console.log("page la", page)
        setOffset(offset)
        dispatch(fetch_post_api(`${process.env.REACT_APP_DEV_API}/api/post/${props.match.params.id}?page=` + page))

    }


    return (
        <div>
            <Grid container className={classes.container}>
                <Grid item className={classes.item} xs={12} sm={3}>
                    <DiscussionDetails discussion_id={props.match.params.id}/>
                </Grid>
                <Grid item xs={12} sm={2}/>

                <Grid item className={classes.item} xs={12} sm={5}>
                    <Paper className={classes.paper}>
                        <PostsBox discussion_id={props.match.params.id}>
                            <PostList/>
                        </PostsBox>


                        <Pagination
                            //requirement is limit only 10 items at a time
                            limit={limit}
                            offset={offset}
                            total={totalDocs}
                            onClick={handleClick}
                        />

                        {token.length > 0 && <FileUpload
                            upload={upload}
                            setPhoto={setPhoto}
                            setShow={setShow}
                            show={show}
                        />}
                    </Paper>
                </Grid>
            </Grid>

        </div>

    );
};

export default withStyles(styles)(PostContainer);
