import React from "react";
import {Router, Route, Switch} from "react-router-dom";
import "./App.css";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import ThemeFile from "./util/ThemeFile";
//components

//pages
import Login from "./pages/Login";
import Signup from "./pages/Signup";
import DiscussionContainer from "./pages/DiscussionContainer";
import history from "./history";
import PostResponsePage from "./components/Post/PostResponsePage";
import Header from "./components/Header";

// make history availble every where in component
import PostContainer from './pages/PostContainer'
import AddBroad from './pages/AddDiscussionContainer'
import PostedPage from "./pages/UserPostContainer";
import {NotificationContainer} from 'react-notifications'
import 'react-notifications/lib/notifications.css';

const theme = createMuiTheme(ThemeFile);


function App() {
    return (
        <MuiThemeProvider theme={theme}>
            <Router history={history}>

                <Header/>

                <Switch>
                    {/* The home page to show all the discussions */}
                    <Route exact path="/" component={DiscussionContainer}/>
                    {/* The login page */}
                    <Route path="/login" component={Login}/>
                    {/* The sign up page */}
                    <Route path="/signup" component={Signup}/>


                    <Route path="/add" component={AddBroad}/>

                    <Route
                        path="/discussion/:id/post/response/:post_id"
                        component={PostResponsePage}
                    />
                    <Route path="/discussion/:id" component={PostContainer}/>
                    <Route path="/posted" component={PostedPage}/>
                </Switch>
            </Router>
            <NotificationContainer/>

        </MuiThemeProvider>
    );
}

export default App;
