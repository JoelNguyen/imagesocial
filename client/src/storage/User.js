// import { Model } from "redux-orm";
const USER_KEY = "storage:user"
class User {

  static save(data) {
    sessionStorage.setItem(USER_KEY, JSON.stringify(data))
  }

  static load() {
    const initialState = {
      token: '',
      username: '',
      exp: '',
      error: ''
    };
    return JSON.parse(sessionStorage.getItem(USER_KEY)) || initialState
  }


  static delete() {
    sessionStorage.removeItem(USER_KEY)
  }

}


export default User;
