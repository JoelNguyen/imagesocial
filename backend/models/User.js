var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// if (mongoose.connection.readyState === 0) {
//   mongoose.connect(require('./connection-string'));
// }
//Huy Code
const bcrypt = require('bcryptjs');


var newSchema = new Schema({
  
  'email': { type: String, required: true },
  'password': { type: String , required: true},
  'name': { type: String , required: true},
  'posted': {type: Number},
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }

  
});

newSchema.pre('save', async function(next){
  // this.updatedAt = Date.now();

  try {
    // Generate a salt
    const salt = await bcrypt.genSalt(10);
    // Generate a password hash (salt + hash)
    const passwordHash = await bcrypt.hash(this.password, salt);
    // Re-assign hashed version over original, plain text password
    this.password = passwordHash;
    console.log('exited');
    next();
  } catch (error) {
    next(error);
  }

});

newSchema.methods.isValidPassword = async function (newPassword) {
  try {
    return await bcrypt.compare(newPassword, this.password);
  } catch (error) {
    throw new Error(error);
  }
}

newSchema.pre('update', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});


// if (!modelAlreadyDeclared()) {
//   const Users = mongoose.model('Users', yourSchema)
// }

// let user
// try {
// user = mongoose.connection.model('User')
// } catch (e) {
// user = mongoose.model('User', newSchema)
// }

// global.newSchema = global.newSchema || mongoose.model('User', newSchema);
// module.exports = global.newSchema;


let User = mongoose.models.User || mongoose.model('User', newSchema);



module.exports = User

// module.exports = user =mongoose.model('User', newSchema);
