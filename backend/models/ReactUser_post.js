var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// if (mongoose.connection.readyState === 0) {
//   mongoose.connect(require('./connection-string'));
// }


var newSchema = new Schema({
  'post_id': { type: Schema.Types.ObjectId, ref: 'Post', required: [true, "Post id is required"] },
  'user': { type: Schema.Types.ObjectId, ref: 'User', required: [true, "User id is required"] },
  'emoji': { type: Schema.Types.ObjectId, ref: 'Emoji', required: [true, "Emoji id is required"] },
  'name': {type: String, required: [true, "Emoji name is required"]},
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function(next){
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});


// let user
// try {
// user = mongoose.connection.model('ReactUser_post')
// } catch (e) {
// user = mongoose.model('ReactUser_post', newSchema)
// }

// module.exports = user

// let Dataset = mongoose.model('ReactUser_post', newSchema);
// module.exports = Dataset

let ReactUser_post = mongoose.models.ReactUser_post || mongoose.model('ReactUser_post', newSchema);



module.exports = ReactUser_post

// global.newSchema = global.newSchema || mongoose.model('ReactUser_post', newSchema);
// module.exports = global.newSchema;


// module.exports = reaction_post = mongoose.model('ReactUser_post', newSchema);
