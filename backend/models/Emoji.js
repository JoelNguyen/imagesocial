var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// if (mongoose.connection.readyState === 0) {
//   mongoose.connect(require('./connection-string'));
// }


var newSchema = new Schema({
  
  'name': { type: String },
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function(next){
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});


// let user
// try {
// user = mongoose.connection.model('Emoji')
// } catch (e) {
// user = mongoose.model('Emoji', newSchema)
// }


// module.exports =user

// let Dataset = mongoose.model('Emoji', newSchema);
// module.exports = Dataset


// global.newSchema = global.newSchema || mongoose.model('Emoji', newSchema);
// module.exports = global.newSchema;

let Emoji = mongoose.models.Emoji || mongoose.model('Emoji', newSchema);



module.exports = Emoji