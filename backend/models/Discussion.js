var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');

// if (mongoose.connection.readyState === 0) {
//   mongoose.connect(require('./connection-string'));
// }


var newSchema = new Schema({

  'title': { type: String, required: [true, "The title of the discussion is required"] },
  'description': { type: String, required:  [true, "The description of the discussion is required"]},
  'posts': [{ type: Schema.Types.ObjectId, ref: 'Post' }],
  'user': {type: Schema.Types.ObjectId, ref: 'User', required: [true, "User is required"]},
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function (next) {
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function () {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function () {
  this.update({}, { $set: { updatedAt: Date.now() } });
});


newSchema.plugin(mongoosePaginate)

// let user
// try {
// user = mongoose.connection.model('Discussion')
// } catch (e) {
// user = mongoose.model('Discussion', newSchema)
// }


// module.exports =user


// let Dataset = mongoose.model('Discussion', newSchema);
// module.exports = Dataset



// global.newSchema = global.newSchema || mongoose.model('Discussion', newSchema);
// module.exports = global.newSchema;


let Discussion = mongoose.models.Discussion || mongoose.model('Discussion', newSchema);



module.exports = Discussion

// module.exports = discussion = mongoose.model('Discussion', newSchema);
