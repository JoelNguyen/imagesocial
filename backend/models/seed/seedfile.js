var mongoose = require('mongoose');

require('../all-models').toContext(global);


//------------------------
// ADD SEEDS BELOW
//------------------------

const Emoji = require('../Emoji')

// suggested module for generating fake contextual data
// var Faker = require('faker');


// For Example

Emoji.create({name: "Like"})
Emoji.create({name: "Hate"})
Emoji.create({name: "Love"})
Emoji.create({name: "Haha"})
Emoji.create({name: "Sad"})
.then((res) => mongoose.connection.close())
.catch((err) => console.log(err))
// CoolUser.create([
//   { name: 'andy', age: 24 },
//   { name: 'alex', age: 23 },
//   { name: Faker.name.firstName(), age: Faker.random.number() }
// ])

// .then(() => {
//   console.log("Seed complete!")  
//   mongoose.connection.close();
// });

// be sure to close the connection once the queries are done

