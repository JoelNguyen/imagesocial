const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

var Schema = mongoose.Schema;
// if (mongoose.connection.readyState === 0) {
//   mongoose.connect(require('./connection-string'));
// }

var newSchema = new Schema({
  'image_url': {
    type: String,
    required: [true, "The url of the image is required"]
  },
  'image_key': {
    type: String,
    required: [true, "The image key is required"]
  },
  'discussion': {
    type: Schema.Types.ObjectId, ref: 'Discussion',
    required: [true, "The discussion board is required"]
  },
  'response': [{
    type: Schema.Types.ObjectId, ref: 'Post'
  }],

  parent_post: { type: Schema.Types.ObjectId, ref: 'Post' },

  'user': { type: Schema.Types.ObjectId, ref: 'User', required: [true, "User is required"] }, 
  // fix schema 
  'reaction': [{
    'emoji': {
      type: Schema.Types.ObjectId,
      ref: 'Emoji',
    },
    'user': { type: Schema.Types.ObjectId, ref: 'User', required: [true, "User is required"] }
  }],
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function (next) {
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function () {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function () {
  this.update({}, { $set: { updatedAt: Date.now() } });
});


newSchema.plugin(mongoosePaginate)


// let user
// try {
// user = mongoose.connection.model('Post')
// } catch (e) {
// user = mongoose.model('Post', newSchema)
// }

// let Dataset = mongoose.model('Post', newSchema);
// module.exports = Dataset

// global.newSchema = global.newSchema || mongoose.model('Post', newSchema);
// module.exports = global.newSchema;
// module.exports = user

let Post = mongoose.models.Post || mongoose.model('Post', newSchema);



module.exports = Post

// module.exports = post = mongoose.model('Post', newSchema);
