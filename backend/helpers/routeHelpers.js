const Joi = require("joi");

module.exports = {
  validateBody: schema => {
    return (req, res, next) => {
      const result = Joi.validate(req.body, schema);
      if (result.error) {
        return res.status(400).json(result.error);
      }

      if (!req.value) {
        req.value = {};
      }
      req.value["body"] = result.value;
      next();
    };
  },

  schemas: {
    authSchema: Joi.object().keys({
      // name: Joi.string().required(),
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string().required()
    }),
    loginSchema: Joi.object().keys({
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string().required()
    }),
    postSchema: Joi.object().keys({
      // discussion_id: Joi.string().required(),
      // photo: Joi.required()
    }),
    ChangeSchema: Joi.object().keys({
      post_id: Joi.string().required(),
      image_key: Joi.string().required(),
      discussion_id: Joi.string().required(),
      photo: Joi.required()
    }),
    DeleteSchema: Joi.object().keys({
        post_id: Joi.string().required(),
        image_key: Joi.string().required(),
        discussion_id: Joi.string().required()
      }),
  }
};
