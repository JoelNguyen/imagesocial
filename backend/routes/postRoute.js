const express = require("express");
const router = express.Router();
const PostController = require("../controller/postController");
const upload = require("../middleware/multer");
const {
    canBeDeleted,
    canBeChanged
} = require("../middleware/postValidator");
// const { body, check, validationResult } = require('express-validator')
const passport = require("passport");

const {validateBody, schemas} = require("../helpers/routeHelpers");
// find all user post
router.get(
    "/user/posted",
    passport.authenticate("jwt", {session: false}),
    PostController.user_post
);

router.get("/detail/parent/response/:post_id", PostController.detail);
router.post(
    "/reaction/:post_id/:discussion_id/:emoji_id",
    passport.authenticate("jwt", {session: false}),
    PostController.reaction
);

router.get(
    "/reaction/find/:post_id",
    passport.authenticate("jwt", {session: false}),
    PostController.user_reaction_post
);

router.get("/:discussion", PostController.get);
// need to set up observer for socket.io when there new post in admin
router.post(
    "/",
    passport.authenticate("jwt", {session: false}),
    upload.single("photo"),
    PostController.post
);

// change post
router.put(
    "/",
    passport.authenticate("jwt", {session: false}),
    //   validateBody(schemas.ChangeSchema),
    canBeChanged,
    upload.single("photo"),
    PostController.put
);

router.delete(
    "/",
    passport.authenticate("jwt", {session: false}),
    canBeDeleted,
    validateBody(schemas.DeleteSchema),
    PostController.delete
);

router.post("/test", PostController.test);

router.post(
    "/response",
    passport.authenticate("jwt", {session: false}),
    upload.single("photo"),
    PostController.response
);

router.get("/response/:discussion/:post", PostController.responseList);

// leaderboard
router.get("/user/leaderboard", PostController.leaderboard);
// get all the emoji
router.get("/reaction/emoji", PostController.emoji);
// router.use()

module.exports = router;
