const express = require('express');
const router = express.Router();
const LabController = require('../controller/lab6Controller')


router.post('/', LabController.get);

router.post('/add', LabController.post);

// router.use()

module.exports = router;