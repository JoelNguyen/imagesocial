const express = require('express');
const router = express.Router();
const DiscussionController = require('../controller/discusstionController')
const passport = require("passport");
const DiscussionMiddleWare = require('../middleware/discussionValidator')
router.get('/', DiscussionController.get)

router.get('/:id', DiscussionController.details)

router.post('/', passport.authenticate("jwt", {session: false}), DiscussionController.post)

router.delete('/', passport.authenticate("jwt", {session: false}), DiscussionMiddleWare.isOwner, DiscussionController.delete)
// need to set up observer for socket.io when there new post in admin

module.exports = router;