const router = require("express-promise-router")();
const passport = require("passport");

const AuthenticationController = require("../controller/authController");

//Huy Codes
const { validateBody, schemas } = require("../helpers/routeHelpers");

router.post(
  "/login",
  validateBody(schemas.loginSchema),
    AuthenticationController.login,
);

router.post(
  "/register",
  // validateBody(schemas.authSchema),
  AuthenticationController.register
);

// router.use()

//huy codes
router.get('/secret',  passport.authenticate("jwt", { session: false }))
// router
//   .route("/secret")
//   .get(
//     // passport.authenticate("jwt", { session: false }),

//   );

module.exports = router;
