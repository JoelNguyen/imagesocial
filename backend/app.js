const express = require('express');

const cookieParser = require('cookie-parser');
const app = express();
const bodyParser = require('body-parser')
const sls = require('serverless-http')
const cors = require('cors')
require('./middleware/passport.js')
// //Huy Codes
const morgan = require('morgan');
// // connect to mongoose db
const mongoose = require('mongoose')
mongoose.connect('mongodb://joelNguyen:Hoilamcho1010@ds139619.mlab.com:39619/imagesocial') //APIAuthentication is the name of the database
// Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());



app.use(cors())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'build')));


app.get("/", (req, res) => {
    res.send('hello')
})


// handle all the post controller
const postsRoute = require('./routes/postRoute')
const discussionRoute = require('./routes/discussionRoute')
const authenticationRoute = require('./routes/authenticationRoute')


app.use('/api/post', postsRoute);
app.use('/api/discussion', discussionRoute)
app.use('/api/auth', authenticationRoute)

// //Huy Code
// //1st step:  Start the server
// const port = process.env.PORT || 3000; //  - Define the port
// app.listen(port);
// console.log(`Server listening at ${port}`);


// mongoose.connection.close()

// module.exports.handler = sls(app, {
//     binary: ['image/png', 'image/jpeg', 'image/jpg', 'multipart/form-data', 'image/*']
// });



module.exports = app
