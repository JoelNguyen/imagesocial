require("dotenv").config();

const multer = require("multer");
const aws = require("aws-sdk");

const multerS3 = require("multer-s3");

aws.config.update({
  accessKeyId: process.env.S3_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.S3_AWS_SECRET_ACCESS_KEY,
  region: process.env.S3_AWS_REGION
});
const s3 = new aws.S3();

const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.S3_AWS_BUCKET_NAME,
    acl: "public-read",
    onError: function(err, next) {
      console.log("here");
      console.log("error", err);
      next(err);
    },
    key: function(req, file, cb) {
      const filename = Date.now().toString() + "-" + file.originalname;
      cb(null, filename);
    }
  })
});

const deleteFile = (filePath) => {
  s3.deleteObject(
    {
      Bucket: process.env.S3_AWS_BUCKET_NAME,
      Key: filePath
    },
    function(err, data) {
        console.log(err)
    }
  );
};

module.exports = upload;
module.exports.deleteFile = deleteFile
