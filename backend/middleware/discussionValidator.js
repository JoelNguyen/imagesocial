const Discussion = require("../models/Discussion");
const isOwner = async (req, res, next) => {
  const discussion_id = req.body.discussion_id;
  const userId = req.user._id;

  try {
    const discussion = await Discussion.findOne({
      _id: discussion_id,
      user: userId
    });

    if (discussion) {
      return next();
    } else {
      return res
        .status(400)
        .json({ error: "You are not allow to delete this discussion" });
    }
  } catch (e) {
    return res.status(400).json({ error: "Can't delete the discussion" });
  }
};

module.exports.isOwner = isOwner;
