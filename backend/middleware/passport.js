const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const LocalStrategy = require("passport-local").Strategy;
const {ExtractJwt} = require("passport-jwt");
const {JWT_SECRET} = require("../configuraton/index");
const User = require("../models/User");

//  JSON WEB TOKEN STRATEGGY
passport.use(
    "jwt",
    new JwtStrategy(
        {
            jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("JWT"),
            secretOrKey: JWT_SECRET
        },
        async (payLoad, done) => {
            try {
                //  find the user specified in token
                const user = await User.findById(payLoad.sub);

                //  if user doesnt exists, handle it
                if (!user) {
                    return done(null, false);
                }
                //  otherwise, return the user

                done(null, user);
            } catch (error) {
                done(error, false);
            }
        }
    )
);

//  LOCAL STRATEGGY
passport.use(
    // "local",
    new LocalStrategy(
        {
            usernameField: "email"
        },
        async (email, password, done) => {
            try {
                // Find the user given the email
                const user = await User.findOne({email});


                // If not, handle it
                if (!user) {
                    return done(null, false);
                }

                // Check if the password is correct
                const isMatch = await user.isValidPassword(password);

                // If not, handle it
                if (!isMatch) {
                    return done(null, false);
                }

                // Otherwise, return the user
                done(null, user);
            } catch (error) {
                done(error, false)
            }
        }
    )
);
