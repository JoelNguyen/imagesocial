const {body, check, validationResult} = require("express-validator");
const ReactUser_post = require("../models/ReactUser_post");
const Post = require("../models/Post");

const validate = validations => {
    return async (req, res, next) => {
        await Promise.all(validations.map(validation => validation.run(req)));
        const errors = validationResult(req);

        if (errors.isEmpty()) {
            return next();
        }
        res.status(422).json({errors: errors.array()});
    };
};


const canBeDeleted = async (req, res, next) => {
    const post_id = req.body.post_id;
    const post = await Post.findOne({_id: post_id});

    if (post) {
        if (post.response.length > 0) {
            return res.status(400).end("This post has response, you can't delete it");
        }
    }

    return next();
};

const canBeChanged = async (req, res, next) => {
    const post_id = req.body.post_id;
    const emoji = await ReactUser_post.find({post_id: post_id});
    console.log("emoji", emoji);
    console.log(post_id);
    if (emoji.length == 0) {
        return next();
    } else {
        return res.status(400).end("This post has reaction, you can't change it");
    }

};



module.exports.validate = validate;

module.exports.validatePost = [
    body("discussion_id").exists(),
    check("photo", "A photo fields is required")
];

module.exports.canBeChanged = canBeChanged;
module.exports.canBeDeleted = canBeDeleted;
