const Cash = require('../models/Cash')
class LabController {
    async get(req, res, next) {
        const data = await Cash.findById(req.body.id);
        res.status(200).send(data)
    }

   async post(req, res, next) {
   
        const data = await Cash.findById(req.body.id)
 

        if(data){
            console.log("update")
            const response = await Cash.findByIdAndUpdate({ _id: req.body.id }, { $inc: { 'money': req.body.money } })
            res.send(response)

        } else {
            console.log("Crete")
            const response = await Cash.create({money: req.body.money})
            res.send(response)
        }
    }
}

module.exports = new LabController();