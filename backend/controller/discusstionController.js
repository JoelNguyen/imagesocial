const Discussion = require('../models/Discussion')

class DiscussionController {
    //Get all the discussions in the database
    async get(req, res) {
        const options = {
            page: req.query.page,
            limit: 10
        };
        try {
            //convert result in pagination
            const discussion = await Discussion.paginate({}, options)
            res.send(discussion)
        } catch (e) {
            res.send(e)
        }
    }

    //Get the detail of specific post
    async details(req, res) {
        try {
            const discussion = await Discussion.findOne({ _id: req.params.id })
            res.send(discussion)
        } catch (e) {
            res.send(e)
        }
    }

    //Store the post (image) in the database
    async post(req, res) {
        const newDiscussion = req.body.discussion
        const userId = req.user._id
        try {
            const discussion = await Discussion.create({
                ...newDiscussion,
                user: userId
            })
            if (discussion) {
                res.status(201).send({
                    "Success": "New discussion has created",
                    discussion
                })
            } else {
                res.status(400).send({
                    "Error": "Cannot create new discussion",
                })
            }

        } catch (e) {
            res.send(e.message)
        }
    }

    //delete the post
    async delete(req, res) {
        const discussion_id = req.body.discussion_id
        try {
            const discussion = await Discussion.findByIdAndDelete(discussion_id)

            res.status(201).send({
                "Success": "Discussion has deleted",
                discussion
            })
        } catch (e) {
            res.status(400).send(e)
        }
    }
}

// create singleton of discussion controller
module.exports = new DiscussionController()