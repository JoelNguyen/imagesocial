//Huy Codes
const JWT = require("jsonwebtoken");
//Bring in User model
const User = require("../models/User");
const { JWT_SECRET } = require("../configuraton/index");

signToken = user => {
  const exp = new Date().setDate(new Date().getDate() + 1);
  const token = JWT.sign(
    {
      iss: "huy",
      sub: user.id,
      iat: new Date().getTime(), //current time
      exp //time the token expired
    },
    JWT_SECRET
  );

  const returnData = {
    token: token,
    username: user.email,
    exp
  };
  return returnData;
};

class AuthenticationController {
  async register(req, res) {
    //Huy Codes
    //check if there is an existing custerm with the same email
    const foundUser = await User.findOne({ email: req.body.email });
    if (foundUser) {
      return res.status(400).send({ error: "Email is already in used" });
    }

    // create a new user
    const { name, email, password } = req.body;
    const newUser = new User({
      name: name,
      email: email,
      password: password
    });

    await newUser.save(); // Save new user to database

    // Generate the token
    let token = signToken(newUser);
    token._id = newUser._id;

    // Response with token
    res.status(200).json({ token: token });
  }

  async login(req, res) {
    try {
      const email = req.body.email;
      const password = req.body.password;
      const user = await User.findOne({ email });

      // If not, handle it
      if (!user) {
        return res
          .status(400)
          .send({ error: "Can't find user with email: " + email });
      }

      // Check if the password is correct
      const isMatch = await user.isValidPassword(password);

      if (!isMatch) {
        return res.status(403).send({ error: "Password is incorrect" });
      }

      // Generate token
      let token = signToken(user);
      token._id = user._id;

      // respond with token
     return res.status(200).json({ data: token });

    } catch (e) {
      return res
        .status(500)
        .send({ error: "Error with login, please try again" });
    }

    // TASk:

    // res.send({
    //     token: "jaowefj23rnoi23f2l3ij",
    //     exp: '12h',
    //     name: 'joel'
    // })
  }

  // verify token
  verify() {}

  secret(req, res) {
    res.send(req.user);
  }
}

// create authentication controller singleton
module.exports = new AuthenticationController();
