const Post = require("../models/Post");
const Discussion = require("../models/Discussion");
const Emoji = require("../models/Emoji");
const container = require("../provider/serviceProvider");
const _ = require("lodash");
require("../middleware/multer");
const User = require("../models/User");
const ReactUser_post = require("../models/ReactUser_post");
const { deleteFile } = require("../middleware/multer");
const {
  SERVICE_REPOSITORY_POST
} = require("../type/repositoryType");


class PostController {
  // GET all the post
  async get(req, res) {
    //get the post dependency
    const post_di = await container.get(SERVICE_REPOSITORY_POST);

    //get the query page if the client request it
    const page = req.query.page || 1;

  
    const discussion_id = req.params.discussion;
    try {
      // Dependency function to get all the post with discussion id
      const data = await post_di.index(discussion_id, page);
      await res.send(data);
    } catch (e) {
      await res
        .status(400)
        .send("Can't find the discussion with id: " + discussion_id);
    }
  }

  //post new image
  async post(req, res) {
    // this code example is from: https://stackoverflow.com/questions/51228059/mongo-db-4-0-transactions-with-mongoose-nodejs-express
    try {
      //check if the file has been upload to AWS S3
      if (req.file) {
        const newPost = {
          image_url: req.file.location,
          discussion: req.body.discussion_id,
          image_key: req.file.key,
          user: req.user._id
        };

        // create new post
        const post = await Post.create(newPost);
        const pushQuery = { $push: { posts: post._id } };
        // update the post uploaded by user
        await User.findByIdAndUpdate(
          { _id: req.user._id },
          { $inc: { posted: 1 } }
        );

        // update the post in discussion 
        await Discussion.findByIdAndUpdate(
          req.body.discussion_id,
          pushQuery
          // { session: session }
        );
        if (post) {
          return res.status(201).send({
            Success: "Upload new post done",
            post
          });

          // commit the transaction
          // await DiscussionSession.commitTransaction();
        } else {
          // about commit
          throw Error("Can't save post");
        }
      } else {
        // abort commit
        throw Error("Can't save post");
      }
    } catch (e) {
      // abort commit
      // await DiscussionSession.abortTransaction();
      deleteFile(req.file.key);

      return res.status(400).send({ Error: "Unable to upload new post" });
    }

    // DiscussionSession.endSession();
  }

  // need to put validation
  async response(req, res) {
    if (req.file && req.body.parent_post) {
      // get the post dependency repo
      const post_di = await container.get(SERVICE_REPOSITORY_POST);
      const newPost = {
        image_url: req.file.location,
        discussion: req.body.discussion_id,
        parent_post: req.body.parent_post,
        image_key: req.file.key,
        user: req.user._id
      };
      try {

        // create new post
        const post = await Post.create(newPost);

        const pushQuery = { $push: { posts: post._id } };

        // add post to the discussion
        await Discussion.findByIdAndUpdate(req.body.discussion_id, pushQuery);

        await User.findByIdAndUpdate(
          { _id: req.user._id },
          { $inc: { posted: 1 } }
        );

        if (post) {
          // add that new post to it parent post
          post_di.add_response_post(req.body.parent_post, post._id);
          res.status(201).send({
            Success: "Upload new post done",
            post
          });
        } else {
          res.status(400).send({ Error: "Unable to save new post" });
        }
      } catch (e) {
        res.status(400).send(e.message);
      }
    } else {
      res.status(400).send({ Error: "Unable to save image" });
    }
  }

  // update the post
  async put(req, res) {
    const post_id = req.body.post_id;
    const user_id = req.user._id;
    // const old_image_key = req.body.image_key;

    const new_image_key = req.file.key;
    const image_url = req.file.location
    console.log(new_image_key, user_id, post_id)
    // const discussion_id = req.body.discussion_id;
    try {
      // find the post with id, user id and key id
      await Post.findOneAndUpdate(
        {
          _id: post_id,
          user: user_id,
          // discussion: discussion_id
        },
        { image_key: new_image_key,  image_url: image_url
              }
      );

      // in the future, we want to keep all the image
      // deleteFile(old_image_key);
      return res.status(200).send({ Success: "Updated" });
    } catch (e) {
      console.log(e);
      return res.status(400).send({
        error:
          "Can't change this post, please check if you have the correct credential or post id"
      });
    }
  }

  //Get the reaction of the post
  async reaction(req, res) {
    const emoji_id = req.params.emoji_id;
    const post_id = req.params.post_id;
    const name = req.body.name;

    // if the user submit the same reaction to image, delete it
    try {
      const deleted = await ReactUser_post.findOneAndDelete({
        emoji: emoji_id,
        user: req.user._id,
        post_id: post_id,
        name: name
      });
      if (deleted) {
        return res.status(201).send("Deleted");
      }
    } catch (e) {}

    // For Create or update the reaction
    try {

      // find the old post
      const oldReaction = await ReactUser_post.findOne({
        post_id: post_id,
        user: req.user._id
      });

      if (oldReaction) {

        // update the reaction if found
        console.log("update", name);
        const response = await ReactUser_post.findOneAndUpdate(
          { post_id: post_id, user: req.user._id },
          { emoji: emoji_id, name: name }
        );
        return res.status(201).send(response);
      } else {

        // create new reaction
        const response = await ReactUser_post.create({
          post_id: post_id,
          user: req.user._id,
          emoji: emoji_id,
          name: name
        });
        return res.status(201).send(response);
      }
    } catch (e) {
      
      return res
        .status(400)
        .send({ error: "Can't make reaction, please try again" });
    }
  }

  //Get all the emoji available
  async emoji(req, res) {
    try {
      const emojis = await Emoji.find({});
      res.status(200).send(emojis);
    } catch (e) {
      res.status(500).send(e);
    }
  }

  // to find the reaction of the user in particular post
  async user_reaction_post(req, res) {
    const user = req.user._id;
    const post_id = req.params.post_id;
    try {
      const reaction = await ReactUser_post.findOne({ post_id: post_id, user });
      return res.status(200).send(reaction);
    } catch (e) {
      res.status(200).send({});
    }
  }


  //Delete the post
  async delete(req, res) {
    const post_id = req.body.post_id;
    const user_id = req.user._id;
    const image_key = req.body.image_key;
    const discussion_id = req.body.discussion_id;
    try {
      // find the post with id, user id and key id
      await Post.findOneAndRemove({
        image_key: image_key,
        _id: post_id,
        user: user_id,
        discussion: discussion_id
      });

      const pullQuery = { $pull: { posts: post_id } };
      await Discussion.update({ _id: discussion_id }, pullQuery);


      deleteFile(image_key);
      return res.status(200).send({ Success: "Deleted" });
    } catch (e) {
      return res.status(400).send({
        error:
          "Can't delete this post, please check if you have the correct credential or post id"
      });
    }
  }

  //for testing purpose of the route
  async test(req, res) {

    const post_di = await container.get(SERVICE_REPOSITORY_POST);

    const post_id = req.body.post_id;

    const emoji_id = req.body.emoji_id;

    // find that reaction of the post
    const { reaction } = await post_di.findReaction(post_id);

    // find that emoji
    const emoji = _.find(reaction, ele => {
      return String(ele.emoji) === emoji_id;
    });

    if (emoji) {
      await post_di.update_reaction_post(post_id, emoji_id, (emoji.count += 1));
    } else {
      const newEmoji = {
        emoji: emoji_id,
        count: 1
      };
      try {
        await post_di.add_reaction_post(post_id, newEmoji);
      } catch (e) {
        res.status(400).send({ error: "fail to update emoji", message: e });
      }
    }

    res.status(201).send({ success: "update success" });
  }

  //Get the response of spefific post
  responseList(req, res) {
    const discussion_id = req.params.discussion;
    const post_id = req.params.post;
    // return res.send(req.params)
    Post.paginate({ parent_post: post_id, discussion: discussion_id })
      .then(data => res.send(data))
      .catch(err => res.send(err));
  }

  //get the detail of specific a post (image)
  async detail(req, res) {
    try {
      const post = await Post.findOne({ _id: req.params.post_id });
      return res.send(post);
    } catch (e) {
      console.log(e)
      return res
        .status(404)
        .send({ error: "Can't found the parent post with id: " + req.params.post_id });
    }
  }

  // find all posted by user
  async user_post(req, res) {
    try {
      const post = await Post.find({ user: req.user._id });
      return res.send(post);
    } catch (e) {
      return res
        .status(404)
        .send({ error: "Can't found the post with id: " + req.params.post_id });
    }
  }

  //get the leaderboard data
  async leaderboard(req, res) {
    try {
      const users = await User.find({}, 'name posted');

      return res.send(users);
    } catch (e) {
      return res.status(404).send({ error: "Can't get the leaderboard" });
    }
  }
}

// create post controller singleton
module.exports = new PostController();
