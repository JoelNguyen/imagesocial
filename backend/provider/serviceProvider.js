const { ContainerBuilder } = require('node-dependency-injection')
const PostRepository = require('../repository/postRepository')
const EmojiRepository = require('../repository/emojiRepository')
const {SERVICE_REPOSITORY_EMOJI, SERVICE_REPOSITORY_POST} = require('../type/repositoryType')
const container =  new ContainerBuilder()

container.register(SERVICE_REPOSITORY_POST, PostRepository)

container.register(SERVICE_REPOSITORY_EMOJI, EmojiRepository)

module.exports = container