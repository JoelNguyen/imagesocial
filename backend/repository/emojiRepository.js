const Emoji = require('../models/Emoji')
module.exports = class EmojiRepository {
    async find(id) {
        return new Promise((resolve, reject) => {
            Emoji.findOne({"_id": id})
            .then((data) => resolve(data))
            .catch((err) => reject(err))
        })
    }
}