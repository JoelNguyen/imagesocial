const Post = require('../models/Post')

class PostRepository {
    index(discussion_id, page) {
        const options = {
            page,
            limit: 10
        };
        const posts = new Promise((resolve, reject) => {
            // Post.find({discussion : discussion_id})
            Post.paginate({discussion: discussion_id, parent_post: null}, options)
                .then((data) => {
                    console.log(data)
                    resolve(data)
                })
                .catch((err) => reject(err))
        })

        return posts
    }

    findReaction(post_id) {
        return new Promise((resolve, reject) => {
            Post.findById(post_id).select('reaction')
                .then((reaction) => {
                    reaction ? resolve(reaction) : resolve([])
                    // resolve(reaction)
                })
                .catch((err) => reject(err))
        })
    }


    add_response_post(parent_id, child_id, user) {
        return new Promise((resolve, reject) => {
            const operation = {"$push": {"response": child_id}}

            Post.findByIdAndUpdate(parent_id, operation)
                .then((res) => resolve(res))
                .catch((err) => reject(err))
        })
    }


    add_reaction_post(post_id, emoji) {


        return new Promise((resolve, reject) => {

            // ReactUser_post.update({_id: id}, obj, {upsert: true, setDefaultsOnInsert: true}, cb);
            const operation = {
                // $set: {
                "$push": {"reaction": emoji}
                // }
            }
            Post.updateOne({_id: post_id}, operation)
                .then((res) => resolve(res))
                .catch((err) => reject(err))

        })
    }

    update_reaction_post(post_id, emoji, count) {
        return new Promise((resolve, reject) => {
            const operation = {
                '$set': {
                    'reaction.$.count': count
                }
            }
            Post.update({_id: post_id, 'reaction.emoji': emoji}, operation)
                .then((data) => resolve(data))
                .catch((err) => reject(err))
        });
    }


}

module.exports = PostRepository